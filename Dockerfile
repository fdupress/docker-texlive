FROM alpine:latest

RUN apk add --no-cache texlive-full biber make poppler-utils && \
    apk upgrade --no-cache && \
    adduser -h /home -D -u 1000 texlive

WORKDIR /home
USER texlive
